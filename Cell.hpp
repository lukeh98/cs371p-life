#ifndef Cell_h
#define Cell_h

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

#include "AbstractCell.hpp"

using namespace std;

template <typename T>
class Handle {
    friend bool operator == (const Handle& lhs, const Handle& rhs) {
        if ((lhs.get() == nullptr) && (rhs.get() == nullptr))
            return true;
        if ((lhs.get() == nullptr) || (rhs.get() == nullptr))
            return false;
        return (*lhs.get() == *rhs.get());}

    private:
        T* _p = nullptr;

    protected:
        T* get () {
            return _p;}

        const T* get () const {
            return _p;}

    public:
        Handle () = default;

        Handle (T* p) {
            _p = p;}

        Handle (const Handle& rhs) {
            if (rhs._p != nullptr)
                _p = rhs._p->clone();}

        Handle& operator = (const Handle& rhs) {
            if (this == &rhs)
                return *this;
            Handle that(rhs);
            swap(that);
            return *this;}

        ~Handle () {
            delete _p;}

        void swap (Handle& rhs) {
            std::swap(_p, rhs._p);}
};

struct Cell : public Handle<AbstractCell> {
    Cell () = default;

    Cell (AbstractCell* p) :
            Handle<AbstractCell> (p)
        {}

    Cell             (const Cell&) = default;
    ~Cell            ()             = default;
    Cell& operator = (const Cell&) = default;
    
    bool isAlive() {
        return get()->isAlive();   
    }
};

#endif