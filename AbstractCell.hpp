#ifndef AbstractCell_h
#define AbstractCell_h

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class AbstractCell {
    bool alive;

public:
    virtual ~AbstractCell () = default;
    
    virtual bool isAlive() = 0;
    
    virtual AbstractCell* clone () const = 0;
};

#endif