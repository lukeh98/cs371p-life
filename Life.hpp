// ---------
// Life.h
// ---------

#ifndef Life_h
#define Life_h

// --------
// includes
// --------

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

template<typename T>
class Life {
private:
    vector<vector<T>> board;
    vector<vector<T>> boardBuffer;
    vector<vector<int>> age;
    int pop;
    
public:
    Life(int rows, int cols, T x) {
        for(int i = 0; i < rows + 2; i++) {
            vector<T> v;
            for(int j = 0; j < cols + 2; j++) {
                x = T(new FredkinCell(false));
                v.push_back(x);
            }
            board.push_back(v);
            boardBuffer.push_back(v);
        }  
        
        vector<int> a;
        for(int i = 0; i < cols + 2; i++)
            a.push_back(0);
        for(int i = 0; i < rows + 2; i++)
            age.push_back(a);
        
        pop = 0;
    }
    
    void addCell(int row, int col) {
        row++;
        col++;
        if(!board[row][col].isAlive())
            pop++;
        board[row][col] = T(new FredkinCell(true));
        boardBuffer[row][col] = T(new FredkinCell(true));
    }
    
    int getNeighborsF(int r, int c) {
        int count = 0;
        if(board[r - 1][c].isAlive())
            count++;
        if(board[r + 1][c].isAlive())
            count++;
        if(board[r][c - 1].isAlive())
            count++;
        if(board[r][c + 1].isAlive())
            count++;
        return count;
    }
    
    int getNeighborsC(int row, int col) {
        int count = 0;
        for(int r = row - 1; r < row + 2; r++) {
            for(int c = col - 1; c < col + 2; c++) {
                if(board[r][c].isAlive())
                    count++;
            }
        }
        if(board[row][col].isAlive())
            count--;
        return count;
    }
    
    void nextGen() {
        for(size_t r = 1; r < board.size() - 1; r++) {
            for(size_t c = 1; c < board[r].size() - 1; c++) {
                int neighbors;
                if(age[r][c] < 2) {
                    neighbors = getNeighborsF(r, c);
                    if(!board[r][c].isAlive()) {
                        if(neighbors == 3 || neighbors == 1) {
                            boardBuffer[r][c] = T(new FredkinCell(true)); 
                            pop++;
                        }
                    }
                    else {
                        if(neighbors == 0 || neighbors == 2 || neighbors == 4) {
                            boardBuffer[r][c] = T(new FredkinCell(false));
                            pop--;
                        }
                        else{
                            age[r][c]++;
                            if(age[r][c] == 2) {
                                board[r][c] = T(new ConwayCell(true));
                            }
                        }
                    }
                }
                else {
                    neighbors = getNeighborsC(r, c);
                    if(!board[r][c].isAlive()) {
                        if(neighbors == 3) {
                            boardBuffer[r][c] = T(new ConwayCell(true));
                            pop++;
                        }
                    }
                    else {
                        if(neighbors < 2 || neighbors > 3) {
                            boardBuffer[r][c] = T(new ConwayCell(false));
                            pop--;
                        }
                    }
                }
            }
        }
        for(size_t r = 1; r < board.size() - 1; r++) {
            for(size_t c = 1; c < board[r].size() - 1; c++) {
                if(boardBuffer[r][c].isAlive() != board[r][c].isAlive())
                    board[r][c] = T(boardBuffer[r][c]);
            }
        }
    }
    
    void print() {
         cout << " Population = " << pop << "." << endl;
        for(size_t r = 1; r < board.size() - 1; r++) {
            for(size_t c = 1; c < board[r].size() - 1; c++) {
                if(board[r][c].isAlive()) {
                    if(age[r][c] < 2)
                        cout << age[r][c];
                    else
                        cout << "*";
                }
                else if(age[r][c] < 2)
                        cout << "-";
                    else
                        cout << ".";
            }
            cout << endl;
        }
    }
};

#endif // Life_h