 g.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

ifeq ($(shell uname -s), Life)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -O3 -I/usr/local/include -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
else ifeq ($(shell uname -p), unknown)
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov
    VALGRIND      := valgrind
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
endif

FILES :=                                  \
    .gitignore                            \
    Life.cpp                           \
    Life.hpp                           \
    makefile                              \
    RunLife.cpp                        \
    RunLife.in                         \
    RunLife.out                        \
    TestLife.cpp						  \
    Life.log                           \
    html                                  \
	# uncomment these four lines when you've created those files
	# you must replace GitLabID with your GitLabID

life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-life-tests.git life-tests

html: Doxyfile Life.hpp
	$(DOXYGEN) Doxyfile

Life.log:
	git log > Life.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	$(DOXYGEN) -g

RunLife: Life.hpp Life.cpp RunLife.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) RunLife.cpp
	$(CXX) $(CXXFLAGS) Life.cpp RunLife.cpp -o RunLife

RunLife.cppx: RunLife
	./RunLife < RunLife.in > RunLife.tmp
	-diff RunLife.tmp RunLife.out

TestLife: Life.hpp Life.cpp TestLife.cpp
	-$(CPPCHECK) Life.cpp
	-$(CPPCHECK) TestLife.cpp
	$(CXX) $(CXXFLAGS) Life.cpp TestLife.cpp -o TestLife $(LDFLAGS)

TestLife.cppx: TestLife
	$(VALGRIND) ./TestLife
	$(GCOV) -b Life.cpp | grep -A 5 "File '.*Life.cpp'"

all: RunLife TestLife

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunLife
	rm -f TestLife

config:
	git config -l

ctd:
	$(CHECKTESTDATA) RunLife.ctd RunLife.in

docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

format:
	$(ASTYLE) Life.cpp
	$(ASTYLE) Life.hpp
	$(ASTYLE) RunLife.cpp
	$(ASTYLE) TestLife.cpp

init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p-Life.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Life.cpp
	git add Life.hpp
	-git add Life.log
	-git add html
	git add makefile
	git add README.md
	git add RunLife.cpp
	git add RunLife.ctd
	git add RunLife.in
	git add RunLife.out
	git add TestLife.cpp
	git commit -m "another commit"
	git push
	git status

run: RunLife.cppx TestLife.cppx

scrub:
	make clean
	rm -f  *.orig
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf Life-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	make clean
	@pwd
	@rsync -r -t -u -v --delete            \
    --include "Life.cpp"                \
    --include "Life.hpp"                \
    --include "RunLife.cpp"             \
    --include "RunLife.ctd"             \
    --include "RunLife.in"              \
    --include "RunLife.out"             \
    --include "TestLife.cpp"            \
    --exclude "*"                          \
    ~/projects/cpp/Life/ .
	@rsync -r -t -u -v --delete            \
    --include "makefile"                   \
    --include "Life.cpp"                \
    --include "Life.hpp"                \
    --include "RunLife.cpp"             \
    --include "RunLife.ctd"             \
    --include "RunLife.in"              \
    --include "RunLife.out"             \
    --include "TestLife.cpp"            \
    --exclude "*"                          \
    . downing@$(CS):cs/git/cs371p-Life/

versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
ifneq ($(shell uname -s), Life)
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
