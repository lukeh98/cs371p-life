#ifndef FredkinCell_h
#define FredkinCell_h

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

#include "AbstractCell.hpp"

using namespace std;

class FredkinCell : public AbstractCell {
private:
    bool alive;
    int age;

public:
    FredkinCell(bool a) {
        alive = a;
        age = 0;
    }
    
    FredkinCell(const FredkinCell& c) {
        alive = c.alive;
        age = c.age;
    }
    
    ~FredkinCell() = default;
    
    bool isAlive() override {
        return alive;
    }
    
    FredkinCell* clone() const override {
        return new FredkinCell(*this);
    }
};

#endif