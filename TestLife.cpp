// ---------------
// TestLife.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream
#include <vector>

#include "gtest/gtest.h"
#include "Life.hpp"

using namespace std;

// -----------
// TestLife
// -----------

// ----
// read
// ----

TEST(VotingFixture, read) {
    }


// -----
// print
// -----

TEST(VotingFixture, print) {
}

// -----
// solve
// -----

TEST(VotingFixture, solve) {
}
