// --------------
// Runlife.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <cmath>
#include <cstdio>

#include "Life.hpp"
#include "Cell.hpp"


// ----
// main
// ----

int main () {
    int numTests;
    string input;
    cin >> numTests;
    for(int t = 0; t < numTests; t++) {
        int rows, cols, numCells;
        cin >> rows;
        cin >> cols;
        cin >> numCells;
        Life<Cell> grid = Life<Cell>(rows, cols, Cell());
        for(int n = 0; n < numCells; n++) {
            int x, y;
            cin >> x;
            cin >> y;
            grid.addCell(x, y);
        }
        int gens, freq;
        cin >> gens;
        cin >> freq;
        cout << "*** Life<Cell> " << rows << "x" << cols << " ***" << endl << endl;
        for(int g = 0; g < gens; g++) {
            if(g % freq == 0) {
                cout << "Generation = " << g << ",";
                grid.print();
                cout << endl;
            }
                
            grid.nextGen();
        }
        if(gens % freq == 0) {
            cout << "Generation = " << gens << ",";
            grid.print();
            cout << endl;
        }  
    }
    return 0;
}
