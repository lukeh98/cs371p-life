#ifndef ConwayCell_h
#define ConwayCell_h

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

#include "AbstractCell.hpp"

using namespace std;

class ConwayCell : public AbstractCell {
private:
    bool alive;

public:
    ConwayCell(bool a) {
        alive = a;
    }
    
    ConwayCell(const ConwayCell& c) {
        alive = c.alive;
    }
    
    ~ConwayCell() = default;
    
    bool isAlive() override {
        return alive;
    }
    
    ConwayCell* clone() const override {
        return new ConwayCell(*this);
    }
};

#endif